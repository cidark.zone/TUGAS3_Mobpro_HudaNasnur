import 'package:http/http.dart' as http;
import 'dart:convert';

class PostResult{
  String id, name, job, created;

  PostResult({
    this.id, this.name, this.job, this.created
  });

  factory PostResult.createPostResult(Map<String, dynamic> object) {
    return PostResult(
      id: object['id'],
      name: object['name'],
      job: object['job'],
      created: object['created'],
    );
  }

  static Future<PostResult> connectToApi(String name, String job) async{
    String apiURLPOST = 'https://reqres.in/api/users/';

    var apiResult = await http.get(apiURLPOST);
    var jsonObject = json.encode(apiResult.body);
    var userData = (jsonObject as Map<String, dynamic>)['data'];

    return PostResult.createPostResult(userData);
  }
}