import 'package:flutter/material.dart';
import 'get_model.dart';
import 'post_model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tugas API',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Tugas API Kelompok Huda'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  UserGet userGet = null;

  @override
  void initState() {
    super.initState();
    UserGet.connectToApiUser('5').then((value) {
      setState(() {
        userGet = value;
      });
    });
  }

  @override
  Widget build(BuildContext contect) {
    return Scaffold(
      backgroundColor: Color(0xffe6f5ff),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(30.0),
            ),
            Text(
              "WELCOME!",
              style: TextStyle(
                  color: Color(0xff0563BA),
                  fontSize: 48,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.all(40.0),
            ),
            Container(
                width: 128,
                height: 128,
                decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                    image: new DecorationImage(
                        fit: BoxFit.fill,
                        image: new NetworkImage((userGet != null)
                            ? userGet.avatar
                            : "https://i.imgur.com/BoN9kdC.png")))),
            Padding(
              padding: const EdgeInsets.all(10.0),
            ),
            Text(
              (userGet != null)
                  ? userGet.firstName + " " + userGet.lastName
                  : "Tidak ada data user",
              style: TextStyle(
                  color: Color(0xff0563BA),
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
            ),
            Text(
              (userGet != null) ? userGet.email : "Tidak ada data",
            )
          ],
        ),
      ),
    );
  }
}
