# <div align="center"><img src="assets/logo/sttb.png" width="300" height="300">

[![Build Status - Cirrus][]][Build status]
[![Gitter Channel][]][Gitter badge]
[![Twitter handle][]][Twitter badge]
</div>

## Tugas Mobile Programming 2

Menampilkan seluruh data yang ada pada objek “data” pada method GET (single user) dari reqres in. Data yang diambil adalah data ID 5, karena kami kelompok 5. Terima Kasih :)

## Anggota Kelompok 5

* [Destriyana Sarizqie (18111193)](https://gitlab.com/DestriyanaS)
* [Huda Nasnur Tajali (18111199)](https://gitlab.com/hudanasgor) - Ketua Kelompok
* [Ivan Syah (18111201)](https://gitlab.com/ivansyah420)
* [Marisa (18111209)](https://gitlab.com/marisa998)
* [Yusuf Abdul Rozzaq (18111235)](https://gitlab.com/usuv.official)

## Documentation

* Layout Tugas API
    <br><img src="assets/ss/SS1.png"  width="340" height="640">

## Terima Kasih

[Flutter logo]: https://raw.githubusercontent.com/flutter/website/master/src/_assets/image/flutter-lockup.png
[flutter.dev]: https://flutter.dev
[Build Status - Cirrus]: https://api.cirrus-ci.com/github/flutter/flutter.svg
[Build status]: https://cirrus-ci.com/github/flutter/flutter/master
[Gitter Channel]: https://badges.gitter.im/flutter/flutter.svg
[Gitter badge]: https://gitter.im/flutter/flutter?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge
[Twitter handle]: https://img.shields.io/twitter/follow/flutterdev.svg?style=social&label=Follow
[Twitter badge]: https://twitter.com/intent/follow?screen_name=usuv_
[FFI]: https://flutter.dev/docs/development/platform-integration/c-interop
[platform channels]: https://flutter.dev/docs/development/platform-integration/platform-channels
[interop example]: https://github.com/flutter/flutter/tree/master/examples/platform_channel
